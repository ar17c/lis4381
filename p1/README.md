> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Alexander Ramos

### Project 1 Requirements:

*Three parts:*

1. Screenshot of running application's first user interface
2. Screenshot of running application's second user interface
3. Screenshot of running Skillsets 7,8,9


| First User Interface | Second User Interface |
| :------------------- | :-------------------: |
![user interface 1](img/resume1.png) | ![user interface 2](img/resume2.png)


| Skill Set 7 | Skill Set 8 | Skill Set 9 |
| :---------- | :---------: | ----------: |
 ![skill set 7](img/ss7.png) | ![skill set 8](img/ss8.png) | ![skill set 9](img/ss9.png) |
