import java.util.Scanner;

public class LargestNumber {
    public static void main(String[] args) {
      System.out.println("Developer: Alexander Ramos");
      System.out.println("Program evaluates largest of two integers.");
      System.out.println("Notes: Program does *not* check for non-numeric characters or non-integer values.\n");

      int num1, num2;

      Scanner scnr = new Scanner(System.in);

      System.out.print("Enter first integer: ");
      num1 = scnr.nextInt();

      System.out.print("Enter second integer: ");
      num2 = scnr.nextInt();

      System.out.println();

      if (num1 == num2) {
        System.out.println("Intgers are equal.");
      }
      else if (num1 > num2) {
        System.out.printf("%d is larger than %d%n", num1, num2);
      }
      else {
        System.out.printf("%d is larger than %d%n", num2, num1);
      }
    }
}
