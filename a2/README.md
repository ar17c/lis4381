> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Alexander Ramos

### Assignment 2 Requirements:

*Three Parts:*

1. Create Healthy Recipes Android App
2. Provide screenshots of completed App
3. Provide screenshots of skill sets

| Healthy Recipes Home | Healthy Recipes Ingredients |
| :------------------- | :-------------------------: |
 ![Android app](img/android1.png)   | ![android app 2](img/android2.png)          |


| Skill Set 1 | Skill Set 2 | Skill set 3 |
| :---------- | :---------: | ----------: |
 ![skill set 1](img/skillset1.png) | ![skill set 2](img/skillset2.png) | ![skill set 3](img/skillset3.png) |
