import java.util.Scanner;
public class EvenOrOdd
{
    public static void main(String[] args)
    {
      //print to screen user prompt
      System.out.println("Developer: Alexander Ramos");
      System.out.println("Program evaluates integers as even or odd.");
      System.out.println("Notes: Program does *not* check for non-numeric characters.");


      //Initialize variables, create Scanner object, capture user input
      int x = 0;
      System.out.print("Enter integer: ");
      Scanner sc = new Scanner(System.in);
      x = sc.nextInt();

      if (x % 2 == 0)
      {
        System.out.println(x + " is an even numbers.");
      }
      else
      {
        System.out.println(x + " is an odd number.");
      }

      }

    }
