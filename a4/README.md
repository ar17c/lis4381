> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Alexander Ramos

### Assignment 4 Requirements:

*Three parts:*

1. Screenshot of main page of web app
2. Screenshot of passed validation
3. Screenshot of failed validation
4. Screenshot of running Skillsets 10,11,12


| Screenshot of Main Page | Screenshot of Passed Validation | |             |
| :------------------- | :-------------------: | :-----------------: |
![user interface 1](img/mainpage.png) | ![user interface 2](img/pass.png) | ![user interface 3](img/fail.png) |


| Skill Set 10 Missing | Skill Set 11 Missing | Skill Set 12 Missing |
