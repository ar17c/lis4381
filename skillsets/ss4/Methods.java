import java.util.Scanner;

public class Methods
{

    public static void getRequirements()
    {
        System.out.println("Developer: Alexander Ramos");
        System.out.println("Program evaluates user-entered characters.");
        System.out.println("Use following characters W or w, C or c, H or h, N or n.");
        System.out.println("Use following decision structures: if...else, and switch");
        System.out.println();
        return;
      }

      public static void decisionStructures()
      {
          final Scanner input = new Scanner(System.in);
          char c = ' ';

          System.out.println("Phone types: W or w (work), C or c (cell), H or h (home), N or n (none).");
          System.out.print("Enter phone type: ");
          c = input.next().charAt(0);

          System.out.println("\nif...else:");
          if (c == 'W' || c == 'w')
          {
            System.out.println("Phone type: work");
          }

          else if (c == 'C' || c == 'c')
          {
            System.out.println("Phone type: cell");
          }

          else if (c == 'H' || c == 'h')
          {
            System.out.println("Phone type: home");
          }

          else if (c == 'N' || c == 'n')
          {
            System.out.println("Phone type: none");
          }

          else
          {
            System.out.println("Incorrect character entry.");
          }

          System.out.println("\nswitch:");
          final char x = Character.toUpperCase(c);
          switch(x){
              case 'W' :
                    System.out.println("Phone type work");
                    break;
                    case 'C' :
                    System.out.println("Phone type: cell");
                    break;
                    case 'H' :
                    System.out.println("Phone type: home");
                    break;
                    case 'N' :
                    System.out.println("Phone type: none");
                    break;
                    default :
                    System.out.println("Incorrect character entry.");
                  }
                  System.out.println();
          }
      }

    
