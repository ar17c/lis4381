> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Alexander Ramos

### Assignment 3 Requirements:

*Five Parts:*

1. Provide screenshot of completed ERD
2. Screenshot of running application's first user interface
3. Screenshot of running application's second user interface
4. Screenshot of running application's third user interface
5. Links to a3.mwb and a3.sql

![Completed ERD Screenshot](img/erd.png)

| First User Interface | Second User Interface | Third User Interface |
| :------------------- | :-------------------: | -------------------: |
![user interface 1](img/concert1.png) | ![user interface 2](img/concert2.png) | ![user interface 3](img/concert3.png) |


| Skill Set 4 | Skill Set 5 | Skill Set 6 |
| :---------- | :---------: | ----------: |
 ![skill set 4](img/ss4.png) | ![skill set 5](img/ss5.png) | ![skill set 6](img/ss6.png) |

[A3 sql Link](docs/a3.sql "My A3 sql link")

[A3 mwb Link](docs/a3.mwb "My A3 mwb link")
