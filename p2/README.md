> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Alexander Ramos

### Project 2 Requirements:

1. Edit and Delete functionality added to petstore table
2. delete gives user prompt to confirm
3. edit process page pulls current data of entry
4. RSS feed of student's choice

### README.ME files should include the following items:

* Screenshot of homepage
* Screenshot of index.php
* Screenshot of editing error
* Screenshot of edited index
* Screenshot of delete prompt
* Screenshot of successful delete
* Screenshot of RSS Feed



| Screenshot of Carousel | Screenshot of Index |
| :------------------- | :-------------------: |
![user interface 1](img/carousel.png) | ![user interface 2](img/index.png) |

| Screenshot of Edit petstore | Screenshot of Successful Edit |
| :------------------- | :-------------------: |
![user interface 1](img/edit_petstore.png) | ![user interface 2](img/successful_edit.png) |

| Screenshot of Delete Record Prompt  | Screenshot of Deleted Record |
| :------------------- | :-------------------: |
![user interface 1](img/delete_prompt.png) | ![user interface 2](img/delete.png) |

| Screenshot of RSS Feed |
| :------------------- |
![user interface 1](img/rss.png) |
