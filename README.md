> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>


# LIS4381 - Mobile Web Application Development

## Alexander Ramos

### LIS 4381 Requirments:

*Course Work Links:*

1. [A1 README.MD](a1/README.md "My A1 README.md file")

    * Install AMPPS
    * Install JDK
    * Install Android Studio and create My First App
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    * Provide git command descriptions

2. [A2 README.MD](a2/README.md "My A2 README.md file")

    * Create Healthy Recipes Android App
    * Provide screenshots of completed App
    * Provide screenshots of skill sets

3. [A3 README.MD](a3/README.md "My A3 README.md file")

    * Provide screenshot of completed ERD
    * Screenshot of running application's first user interface
    * Screenshot of running application's second user interface
    * Screenshot of running application's third user interface
    * Links to a3.mwb and a3.sql



4. [A4 README.MD](a4/README.md "My A4 README.md file")

    * Provide screenshot of Main Page
    * Provide screenshot of passed validation
    * Provide screenshot of failed validation
    * Provide screenshots of skill sets 10,11,12


5. [A5 README.MD](a5/README.md "My A5 README.md file")

    * Database basic server side validation
    * Screenshots of populated table, error page
    * Skillsets 12-15

6. [P1 README.MD](p1/README.md "My P1 README.md file")

    * Screenshot of running application's first user interface
    * Screenshot of running application's second user interface
    * Screenshots of running Skillsets 7, 8, 9


7. [P2 README.MD](p2/README.md "My P2 README.md file")

    * Edit/ Delete functionality for A5 table
    * RSS Feed
